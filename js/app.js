$(function(){
	$('.filters select').customSelect();
	$('.custom-select').customSelect({customClass: "listSelect"});

	window.remodalGlobals = {
		namespace: "modal",
		defaults: {
			hashTracking: true
		}
	};

	$(document).on('close', '.remodal', function (e) {
		$('.remodal').find('input').val("");
	});
})